<?php get_template_part('parts/header'); the_post(); ?>

<main>

  <?php get_template_part('parts/page', 'header');?>

  <section class="single padding--bottom">

    <div class="wrap hpad">

      <article <?php post_class(); ?> itemscope itemtype="http://schema.org/BlogPosting">

        <div itemprop="articleBody">
          <?php the_content(); ?>

          <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium' );?>
          <img src="<?php echo esc_url($thumb[0]); ?>" alt="<?php echo $thumb['alt']; ?>">
        </div>

      </article>

    </div>

  </section>

</main>

<?php get_template_part('parts/footer'); ?>