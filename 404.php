<?php get_template_part('parts/header'); ?>

<main>

   <?php get_template_part('parts/page', 'header');?>

  <section class="404 padding--bottom">
  	<div class="wrap hpad clearfix">

    	<h1>Side ikke fundet</h1>
    	<p>Siden du leder efter kan desværre ikke findes</p>

    </div>

  </section>

</main>

<?php get_template_part('parts/footer'); ?>