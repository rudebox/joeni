<?php get_template_part('parts/header'); ?>

<main>

  <?php get_template_part('parts/content', 'layouts'); ?>
  <?php get_template_part('parts/news', 'feed'); ?>
  <?php get_template_part('parts/contact', 'form'); ?>

</main>

<?php get_template_part('parts/footer'); ?>
