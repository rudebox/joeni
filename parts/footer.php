<footer class="footer gray-dark--bg" id="footer" itemscope itemtype="http://schema.org/WPFooter">
	<div class="wrap hpad clearfix">
		<div class="row">
			<?php 
				if (have_rows('footer_columns', 'options') ) : while (have_rows('footer_columns', 'options') ) :
					the_row();
				$title = get_sub_field('column_title');
				$text = get_sub_field('column_text');
			 ?>

			 <div class="col-sm-4 footer__item">
			 	<h4 class="footer__title"><?php echo esc_html($title); ?></h4>
				<?php echo $text; ?>
			 </div>

			<?php endwhile; endif; ?>

			 <div class="col-sm-4 footer__item">
			 	<h4 class="footer__title">Links</h4>
				<?php 
					ob_start();
						scratch_footer_nav(); 
					$out = ob_get_clean();

					echo $out;
				?>
			 </div>

		</div>
	</div>
</footer>

<?php wp_footer(); ?>

</body>
</html>
