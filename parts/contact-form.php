<?php 
//Description: Lionlab contact-form field group layout

//sections settings
$bg = get_field('form_bg', 'options');
$id = get_field('form_id', 'options');
$title = get_field('form_title', 'options');

?>

<section class="contact-form blue--bg padding--both">
	<div class="contact--bg">
	<?php echo file_get_contents(esc_url($bg['url'])); ?>
	</div>
	<div class="wrap hpad">
		<h2 class="contact__header center"><?php echo esc_html($title); ?></h2>
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<?php  
					gravity_form( $id, $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = false, $tabindex, $echo = true );
				?>

			</div>
		</div>
	</div>
</section>