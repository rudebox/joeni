<?php

/*
 * Template Name: Contact
 */

get_template_part('parts/header'); the_post(); 
$text = get_field('contact_text'); 

?>

<main>

  <?php get_template_part('parts/page', 'header');?>

  <section class="contact padding--both">

  	<div class="wrap hpad clearfix">
  		<div class="row">

			<div class="col-sm-6 contact__text">
				<?php echo $text; ?>		
			</div>

			<div class="col-sm-6">
				<?php  
					gravity_form( 3, $display_title = false, $display_description = false, $display_inactive = false, $field_values = null, $ajax = false, $tabindex, $echo = true );
				?>
			</div>

		</div>
	</div>
  </section>

</main>

<?php get_template_part('parts/footer'); ?>