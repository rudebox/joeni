<?php 
/**
* Description: Lionlab link-boxes repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$header = get_sub_field('header');

if (have_rows('linkbox') ) :
?>

<section class="link-boxes <?php echo $bg; ?>--bg padding--<?php echo $margin; ?>">
	<div class="wrap--fluid">
		<?php if ($header) : ?>
		<h2 class="link-boxes__header"><?php echo esc_html($header); ?></h2>
		<?php endif; ?>
		<div class="flex flex--wrap">
			<?php while (have_rows('linkbox') ) : the_row(); 
				$title = get_sub_field('title');
				$text = get_sub_field('text');
				$icon = get_sub_field('icon');
				$link = get_sub_field('link');
			?>

			<a href="<?php echo esc_url($link); ?>" class="col-sm-4 link-boxes__item">

				<div class="link-boxes__wrap">
				<?php if ($icon) : ?>
					<img class="link-boxes__img" src="<?php echo esc_url($icon['sizes']['services']); ?>" alt="<?php echo esc_attr($icon['alt']); ?>">
					<?php endif; ?>
					<span class="link-boxes__icon"><i class="fas fa-share"></i></span>
				</div>

				<div class="link-boxes__content">
					<h3 class="link-boxes__title"><?php echo esc_html($title); ?></h3>
					<?php echo $text; ?>
				</div>
			</a>
			<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; ?>