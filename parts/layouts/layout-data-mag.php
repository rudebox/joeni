<?php 
/**
* Description: Lionlab data-mag repeater field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//sections settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');
$header = get_sub_field('header');

if (have_rows('data_mag') ) :
?>

<section class="data-mag <?php echo $bg; ?>--bg padding--<?php echo $margin; ?>">
	<div class="wrap hpad">
		<?php if ($header) : ?>
		<h2 class="data-mag__header"><?php echo esc_html($header); ?></h2>
		<?php endif; ?>
		<div class="row flex flex--wrap">
			<?php while (have_rows('data_mag') ) : the_row(); 
				$file = get_sub_field('pdf');
				$thumb = get_sub_field('pdf_thumbnail');

				$icon = $file['icon'];
				$name = $file['title']; 
			?>

			<a target="_blank" href="<?php echo esc_url($file['url']); ?>" class="col-sm-4 data-mag__item center" itemscope itemtype="DigitalDocument">

				<img class="data-mag__thumb" src="<?php echo esc_url($thumb['sizes']['thumbnail']); ?>" alt="<?php echo esc_attr($thumb['alt']); ?>"> 
				
				<p class="data-mag__content"><?php echo esc_html($name); ?><br>
				<span class="btn btn--red data-mag__btn">Download PDF</span></p>
			</a>
			<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; ?>