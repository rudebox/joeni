<?php 
/**
* Description: Lionlab gallery field group layout
*
* @package Lionlab
* @subpackage Lionlab
* @since Version 1.0
* @author Kaspar Rudbech
*/

//gallery
$gallery = get_sub_field('gallery'); 
$index = get_row_index();

//section settings
$bg = get_sub_field('bg');
$margin = get_sub_field('margin');

if ( $gallery ) : ?> 

	<section class="gallery <?php echo $bg; ?>--bg padding--<?php echo $margin; ?>" itemscope itemtype="ImageGallery">
		<div class="wrap hpad">

			<div class="row gallery__list flex flex--wrap">

				<?php
					// Loop through gallery
					foreach ( $gallery as $image ) : 
				?>


					<div class="gallery__item" itemprop="associatedMedia" itemscope itemtype="http://schema.org/ImageObject">
						<a href="<?= $image['sizes']['large']; ?>" class="js-zoom gallery__link" data-fancybox="gallery-<?= $index; ?>" itemprop="contentUrl" data-caption="<?= $image['caption']; ?>" title="<?= $image['title'] ?>" data-width="<?= $image['sizes']['large-width']; ?>" data-height="<?= $image['sizes']['large-height']; ?>">
					
						<img class="gallery__image" data-src="<?= $image['sizes']['gallery']; ?>" src="<?= $image['sizes']['gallery']; ?>" alt="<?= $image['alt']; ?>" itemprop="thumbnail" height="<?= $image['sizes']['large-height']; ?>" width="<?= $image['sizes']['large-width']; ?>">
						</a>
					</div>
				<?php endforeach; ?>

			</div>
		</div>
	</section>

<?php endif; ?>