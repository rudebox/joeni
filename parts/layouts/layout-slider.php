<?php
/**
 * Description: Lionlab sliders
 *
 * @package Lionlab
 * @subpackage Lionlab
 * @since Version 1.0
 * @author Kaspar Rudbech
*/

$phone = get_field('phone', 'options');

if ( have_rows('slides') ) : ?>

  <section class="slider">
    <div class="slider__track is-slider">

      <?php
      // Loop through slides
      while ( have_rows('slides') ) :
        the_row();
        $image   = get_sub_field('slides_bg');
        $title = get_sub_field('slides_title');
        $caption = get_sub_field('slides_text'); ?>

        <div class="slider__item flex flex--valign" style="background-image: url(<?php echo esc_url($image['url']); ?>);">
          <div class="wrap hpad slider__container">
            <div class="slider__text center">
              <h2 class="slider__title h1"><?php echo $title; ?></h2>
              <?php echo $caption; ?>
              <a class="btn btn--red btn--phone" href="tel:<?php echo get_formatted_phone(esc_html($phone)); ?>"><?php echo esc_html($phone); ?><i class="fas fa-phone"></i></a>
            </div>
          </div>
        </div>

      <?php endwhile; ?>

    </div>
  </section>
<?php endif; ?>