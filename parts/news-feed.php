<?php 
//Description: Lionlab news-feed field group layout

$bg = get_field('fb_img', 'options');
$title = get_field('fb_title', 'options');

// Custom WP query query
$args_query = array(
	'posts_per_page' => 3,
	'order' => 'DESC',
	'cat' => 4,
);


$query = new WP_Query( $args_query );
?>

<?php if ($query->have_posts() ) : ?>

<section class="news-feed padding--both" style="background-image: url(<?php echo esc_url($bg['url']); ?>)">
	<div class="wrap hpad">
		<?php if ($title) : ?>
		<h2 class="news-feed__header"><?php echo esc_html($title); ?></h2>
		<?php endif; ?>

		<div class="row">
			<?php  while ( $query->have_posts() ) : $query->the_post(); ?>
				
				<div class="col-sm-4 news-feed__item" itemscope itemtype="http://schema.org/BlogPosting">
					<?php the_post_thumbnail('gallery', [ 'alt' => esc_html (get_the_title() )]); ?>
					<?php the_excerpt(); ?>

					<a target="_blank" href="https://www.facebook.com/9490joeni/" class="btn btn--red" href="">Læs mere<i class="fas fa-angle-right"></i></a>
				</div>

			<?php wp_reset_postdata(); ?>
			<?php endwhile; ?>
		</div>
	</div>
</section>
<?php endif; ?>